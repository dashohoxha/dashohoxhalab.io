---
layout:  post
title:   Deduplicating Data With XFS And Reflinks
date:    2019-08-30

summary: Copy-on-Write filesystems have the nice property that it is
    possible to "clone" files instantly, by having the new file refer
    to the old blocks, and copying (possibly) changed blocks. This
    both saves time and space, and can be very beneficial in a lot of
    situations (for example when working with big files). In Linux
    this type of copy is called "reflink". We will see how to use it
    on the XFS filesystem.

tags:    filesystem Copy-on-Write XFS reflinks deduplicate
---
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>Deduplicating Data With XFS And Reflinks</title>
<!-- 2020-02-20 Thu 14:59 -->
<meta  http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta  name="generator" content="Org-mode" />
<meta  name="author" content="Dashamir Hoxha" />
<style type="text/css">
 <!--/*--><![CDATA[/*><!--*/
  .title  { text-align: center; }
  .todo   { font-family: monospace; color: red; }
  .done   { color: green; }
  .tag    { background-color: #eee; font-family: monospace;
            padding: 2px; font-size: 80%; font-weight: normal; }
  .timestamp { color: #bebebe; }
  .timestamp-kwd { color: #5f9ea0; }
  .right  { margin-left: auto; margin-right: 0px;  text-align: right; }
  .left   { margin-left: 0px;  margin-right: auto; text-align: left; }
  .center { margin-left: auto; margin-right: auto; text-align: center; }
  .underline { text-decoration: underline; }
  #postamble p, #preamble p { font-size: 90%; margin: .2em; }
  p.verse { margin-left: 3%; }
  pre {
    border: 1px solid #ccc;
    box-shadow: 3px 3px 3px #eee;
    padding: 8pt;
    font-family: monospace;
    overflow: auto;
    margin: 1.2em;
  }
  pre.src {
    position: relative;
    overflow: visible;
    padding-top: 1.2em;
  }
  pre.src:before {
    display: none;
    position: absolute;
    background-color: white;
    top: -10px;
    right: 10px;
    padding: 3px;
    border: 1px solid black;
  }
  pre.src:hover:before { display: inline;}
  pre.src-sh:before    { content: 'sh'; }
  pre.src-bash:before  { content: 'sh'; }
  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }
  pre.src-R:before     { content: 'R'; }
  pre.src-perl:before  { content: 'Perl'; }
  pre.src-java:before  { content: 'Java'; }
  pre.src-sql:before   { content: 'SQL'; }

  table { border-collapse:collapse; }
  caption.t-above { caption-side: top; }
  caption.t-bottom { caption-side: bottom; }
  td, th { vertical-align:top;  }
  th.right  { text-align: center;  }
  th.left   { text-align: center;   }
  th.center { text-align: center; }
  td.right  { text-align: right;  }
  td.left   { text-align: left;   }
  td.center { text-align: center; }
  dt { font-weight: bold; }
  .footpara:nth-child(2) { display: inline; }
  .footpara { display: block; }
  .footdef  { margin-bottom: 1em; }
  .figure { padding: 1em; }
  .figure p { text-align: center; }
  .inlinetask {
    padding: 10px;
    border: 2px solid gray;
    margin: 10px;
    background: #ffffcc;
  }
  #org-div-home-and-up
   { text-align: right; font-size: 70%; white-space: nowrap; }
  textarea { overflow-x: auto; }
  .linenr { font-size: smaller }
  .code-highlighted { background-color: #ffff00; }
  .org-info-js_info-navigation { border-style: none; }
  #org-info-js_console-label
    { font-size: 10px; font-weight: bold; white-space: nowrap; }
  .org-info-js_search-highlight
    { background-color: #ffff00; color: #000000; font-weight: bold; }
  /*]]>*/-->
</style>
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2013 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>
</head>
<body>
<div id="content">
<h1 class="title">Deduplicating Data With XFS And Reflinks</h1>
<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Create a virtual block device</a></li>
<li><a href="#sec-2">2. Create an XFS filesystem with the 'reflink' flag</a></li>
<li><a href="#sec-3">3. Copy files with '&#x2013;reflink'</a></li>
<li><a href="#sec-4">4. Deduplicate existing files</a></li>
<li><a href="#sec-5">5. Clean up</a></li>
<li><a href="#sec-6">6. References</a></li>
</ul>
</div>
</div>
<p>
Copy-on-Write filesystems have the nice property that it is possible
to "clone" files instantly, by having the new file refer to the old
blocks, and copying (possibly) changed blocks. This both saves time
and space, and can be very beneficial in a lot of situations (for
example when working with big files). In Linux this type of copy is
called "reflink". We will see how to use it on the XFS filesystem.
</p>

<div id="outline-container-sec-1" class="outline-2">
<h2 id="sec-1"><span class="section-number-2">1</span> Create a virtual block device</h2>
<div class="outline-text-2" id="text-1">
<p>
Linux supports a special block device called the loop device, which
maps a normal file onto a virtual block device. This allows for the
file to be used as a "virtual file system". Let's create such a file:
</p>
<pre class="example">
$ fallocate -l 1G disk.img
$ du -hs disk.img
1001M	disk.img
</pre>

<p>
Now let's create a loop device with this file:
</p>
<pre class="example">
$ sudo losetup -f disk.img
$ losetup -a
/dev/loop0: []: (/home/user/disk.img)
</pre>

<p>
The option <b>-f</b> finds an unused loop device, and <code>losetup -a</code> shows
the name of the loop device that was created.
</p>
</div>
</div>

<div id="outline-container-sec-2" class="outline-2">
<h2 id="sec-2"><span class="section-number-2">2</span> Create an XFS filesystem with the 'reflink' flag</h2>
<div class="outline-text-2" id="text-2">
<p>
Let's create an XFS filesystem on it, with the flag <b>reflink=1</b> and
the label <b>test</b>:
</p>
<pre class="example">
$ mkfs.xfs -m reflink=1 -L test disk.img
meta-data=disk.img               isize=512    agcount=4, agsize=64000 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=0, rmapbt=0, reflink=1
data     =                       bsize=4096   blocks=256000, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=1850, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
</pre>

<p>
Now we can mount it on a directory:
</p>
<pre class="example">
$ mkdir mnt
$ sudo mount /dev/loop0 mnt
$ df -h mnt/
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M   40M  954M   4% /home/user/mnt
</pre>
</div>
</div>

<div id="outline-container-sec-3" class="outline-2">
<h2 id="sec-3"><span class="section-number-2">3</span> Copy files with '&#x2013;reflink'</h2>
<div class="outline-text-2" id="text-3">
<p>
Let's create for testing a file of size 100 MB (with random data):
</p>
<pre class="example">
$ sudo chown mnt: user
$ cd mnt/
$ dd if=/dev/urandom of=test bs=1M count=100
100+0 records in
100+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0,445498 s, 235 MB/s
</pre>

<p>
The command <code>df -h .</code> shows us <b>140M</b> used:
</p>
<pre class="example">
$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  140M  854M  15% /home/user/mnt
</pre>

<p>
Let’s copy the file with reflinks enabled:
</p>
<pre class="example">
$ cp -v --reflink=always test test1
'test' -&gt; 'test1'

$ ls -hsl
total 200M
100M -rw-rw-r-- 1 user user 100M Aug 30 10:44 test
100M -rw-rw-r-- 1 user user 100M Aug 30 10:49 test1

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  140M  854M  15% /home/user/mnt
</pre>

<p>
So, each copy of the file is <b>100M</b>, but both of them still take on
disk the same amout of space as before (<b>140M</b>). This shows the
space-saving feature of reflinks. If the file was big enough, we would
have noticed as well that the reflink copy takes no time at all, it is
done instantly.
</p>
</div>
</div>

<div id="outline-container-sec-4" class="outline-2">
<h2 id="sec-4"><span class="section-number-2">4</span> Deduplicate existing files</h2>
<div class="outline-text-2" id="text-4">
<p>
If there were already normal (non-reflink) copies of the file, we
could deduplicate them with a tool like <code>duperemove</code>:
</p>
<pre class="example">
$ cp test test2
$ cp test test3

$ ls -hsl
total 400M
100M -rw-rw-r-- 1 user user 100M Aug 30 10:44 test
100M -rw-rw-r-- 1 user user 100M Aug 30 10:49 test1
100M -rw-rw-r-- 1 user user 100M Aug 30 11:03 test2
100M -rw-rw-r-- 1 user user 100M Aug 30 11:03 test3

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  340M  654M  35% /home/user/mnt
</pre>

<p>
Now let's install and run <code>duperemove</code>
</p>
<pre class="example">
$ sudo apt install duperemove

$ duperemove -hdr --hashfile=/tmp/test.hash .

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  198M  796M  20% /home/user/mnt
</pre>

<p>
So, it has reduced the amount of disk space used from <b>340M</b> to
<b>198M</b>.
</p>
</div>
</div>


<div id="outline-container-sec-5" class="outline-2">
<h2 id="sec-5"><span class="section-number-2">5</span> Clean up</h2>
<div class="outline-text-2" id="text-5">
<p>
Unmount and delete the test directory <code>mnt/</code>:
</p>
<pre class="example">
$ cd ..
$ umount mnt/
$ rmdir mnt/
</pre>

<p>
Delete the loop device:
</p>
<pre class="example">
$ losetup -a
$ sudo losetup -d /dev/loop0
</pre>

<p>
Remove the file that was used to create the loop device:
</p>
<pre class="example">
$ rm disk.img
</pre>
</div>
</div>

<div id="outline-container-sec-6" class="outline-2">
<h2 id="sec-6"><span class="section-number-2">6</span> References</h2>
<div class="outline-text-2" id="text-6">
<ul class="org-ul">
<li><a href="https://www.thegeekdiary.com/how-to-create-virtual-block-device-loop-device-filesystem-in-linux/">https://www.thegeekdiary.com/how-to-create-virtual-block-device-loop-device-filesystem-in-linux/</a>
</li>
<li><a href="https://hooks.technology/2018/03/xfs-deduplication-with-reflinks/">https://hooks.technology/2018/03/xfs-deduplication-with-reflinks/</a>
</li>
</ul>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="date">Date: 2019-08-30</p>
<p class="author">Author: Dashamir Hoxha</p>
<p class="date">Created: 2020-02-20 Thu 14:59</p>
<p class="creator"><a href="http://www.gnu.org/software/emacs/">Emacs</a> 25.2.2 (<a href="http://orgmode.org">Org</a> mode 8.2.10)</p>
<p class="validation"><a href="http://validator.w3.org/check?uri=referer">Validate</a></p>
</div>
</body>
</html>
