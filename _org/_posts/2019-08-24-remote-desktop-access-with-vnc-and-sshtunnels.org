#+TITLE:     Remote Desktop Access With VNC And SSH Tunnels
#+AUTHOR:    Dashamir Hoxha
#+EMAIL:     dashohoxha@gmail.com
#+DATE:  2019-08-24
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:nil LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:overview toc:t ltoc:t mouse:#aadddd buttons:0 path:js/org-info.js
#+STYLE: <link rel="stylesheet" type="text/css" href="css/org-info.css" />
#+begin_comment yaml-front-matter
---
layout:  post
title:   Remote Desktop Access With VNC And SSH Tunnels
date:    2019-08-24

summary: In this article we will discuss about how to access a
    RaspberryPi desktop remotely with VNC. We will use a VPS with
    Docker Scripts and sshtunnels as an intermediary between the
    server and the client, in order to enable a secure communication
    between them.

tags:    RaspberryPi Desktop RealVNC VNC Docker-Scripts sshtunnels
---
#+end_comment

In this article we will discuss about how to access a RaspberryPi
desktop remotely with VNC. We will use a VPS with Docker Scripts and
[[https://gitlab.com/docker-scripts/sshtunnels][sshtunnels]] as an intermediary between the server and the client, in
order to enable a secure communication between them.

* Enable desktop sharing on RaspberryPi

This article explains in details how to install and start a VNC server
on RaspberryPi:
https://www.raspberrypi.org/documentation/remote-access/vnc/

1. Make sure that =realvnc-vnc-server= is installed:
   #+begin_example
   sudo apt update
   sudo apt install realvnc-vnc-server
   #+end_example

2. Enable the VNC server:
   - Run =sudo raspi-config=
   - Navigate to **Interfacing Options**.
   - Scroll down and select **VNC > Yes**

* Install sshtunnels in an intermediary VPS

The installation steps are listed here:
https://gitlab.com/docker-scripts/sshtunnels#installation

1. Install **docker**:
   #+begin_example
   curl -fsSL https://get.docker.com -o get-docker.sh
   sh get-docker.sh
   #+end_example

2. Install **docker-scripts**:
   #+begin_example
   apt install m4 git
   git clone https://gitlab.com/docker-scripts/ds /opt/docker-scripts/ds
   cd /opt/docker-scripts/ds/
   make install
   #+end_example

3. Install **sshtunnels**:
   #+begin_example
   ds pull sshtunnels
   ds init sshtunnels @sshtunnels
   cd /var/ds/sshtunnels/
   vim settings.sh
   ds make
   #+end_example


* Create and use a tunnel

1. On the VPS server create a tunnel for the port =5900= on our
   RaspberryPi:
   #+begin_example
   cd /var/ds/sshtunnels/
   ds tunnel-add my-rpi 5900
   #+end_example
   This command will create the directory ~tunnels/my-rpi.5900/~.

2. Copy to the RaspberryPi the script
   ~tunnels/my-rpi.5900/share-port-5900.sh~, and run it like this:
   #+begin_example
   chmod 700 share-port-5900.sh
   ./share-port-5900.sh
   #+end_example
   This will open a ssh tunnel for port =5900=, from the RaspberryPi to
   the VPS.  It will also create the cron job
   =/etc/cron.d/share-port-5900=, to check and make sure periodically
   (each minute) that this tunnel is open.

3. Copy on a client computer the script
   ~tunnels/my-rpi.5900/connect-to-my-rpi-5900.sh~ and run it like
   this:
   #+begin_example
   chmod 700 connect-to-my-rpi-5900.sh
   ./connect-to-my-rpi-5900.sh 5900
   #+end_example
   This will open a tunnel for the port =5900= from our client
   computer to the VPS. The intermediary VPS will connect both tunnels
   (from RPi and from the client), and the result will be that if we
   open =localhost:5900= on a VNC viewer on the client, we will
   actually be accessing the port =5900= on the RaspberryPi.

4. If we now run the command =./connect-to-my-rpi-5900.sh 5901= on the
   client, then instead of using =localhost:5900= (on a VNC viewer),
   we should use =localhost:5901= (and again it will access the port
   =5900= on RaspberryPi).

* Close and destroy a tunnel

- On RaspberryPi run:
  #+begin_example
  ./share-port-5900.sh stop
  #+end_example
  This will close the tunnel and also delete the cron job
  ~/etc/cron.d/share-port-5900~.

- On the client run:
  #+begin_example
  ./connect-to-my-rpi-5900.sh stop
  #+end_example

- On the VPS server run:
  #+begin_example
  cd /var/ds/sshtunnels/
  ds tunnel-del my-rpi 5900
  #+end_example
  This will delete the directory ~tunnels/my-rpi.5900/~.
